﻿using System.Windows;
using DesktopClient.AlphaVDataProvider;
using DesktopClient.ViewModel;
using ServiceStack;

namespace DesktopClient
{
    public partial class App : Application
    {
        protected async override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var mainWindow = new MainWindow();
            mainWindow.Show();

        }      
    }
}
