﻿using AlphaVantage.Net.Stocks;
using AlphaVantage.Net.Stocks.TimeSeries;
using ServiceStack;
using System.Threading.Tasks;

namespace DesktopClient.AlphaVDataProvider
{
    class AlphaVintageStockDataProvider
    {
        AlphaVantageStocksClient client = null;
        string apiKey = "ZKWZRPFJ8DEOQWTJ";
        public AlphaVintageStockDataProvider()
        {
            client = new AlphaVantageStocksClient(apiKey);
        }

        public async Task<StockTimeSeries> GetDailyTimeSeriesAsync(string ticker) => await client.RequestDailyTimeSeriesAsync(ticker, TimeSeriesSize.Full, false);

        public async Task<StockTimeSeries> GetIntradayTimeSeriesAsync(string ticker) => await client.RequestIntradayTimeSeriesAsync(ticker, IntradayInterval.OneMin, TimeSeriesSize.Full);

        public async Task<StockTimeSeries> GetWeeklyTimeSeriesAsync(string ticker) => await client.RequestWeeklyTimeSeriesAsync(ticker, true);

        public async Task<StockTimeSeries> GetMonthlyTimeSeriesAsync(string ticker) => await client.RequestMonthlyTimeSeriesAsync(ticker, true);

        public string GetDailyBatchTimeSeries(string ticker)
        {
            string url = string.Format("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={0}&outputsize=full&apikey=apiKey", ticker);
            var apiResponse = url.GetStringFromUrl();
            return apiResponse;
        }
        public string GetEndQuoteTimeSeries(string ticker)
        {
            string url = string.Format("https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={0}&apikey=apiKey", ticker);
            var apiResponse = url.GetStringFromUrl();
            return apiResponse;
        }
    }
}
