﻿using System.Collections.Generic;
using System.Text;
using System;
using System.Diagnostics;
using Intrinio.SDK.Api;
using Intrinio.SDK.Client;
using Intrinio.SDK.Model;

namespace DesktopClient.Financials
{
    public class IntrinioStockDataProvider
    {

        SecurityApi securityApi = null;

        public IntrinioStockDataProvider()
        {
            Configuration.Default.AddApiKey("api_key", "OjlkODNjYWY3MzdiYWU4NDY3NjI3MTNjYjI4ZDdiMGJl");
            securityApi = new SecurityApi();
        }

        public ApiResponseSecurityStockPrices GetSecurityStockPrices(string identifier, DateTime? startDate = null, DateTime? endDate = null, string frequency = null, int? pageSize = null, string nextPage = null)
        {
            ApiResponseSecurityStockPrices response = null;
            try
            {
                return response = securityApi.GetSecurityStockPrices(identifier, startDate, endDate, frequency, pageSize, nextPage);

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception when calling SecurityApi.GetSecurityStockPrices: " + e.Message);
                return response;
            }

        } 
        public ApiResponseSecurityIntradayPrices GetSecurityIntradayPrices(string identifier, string? source = null, DateTime? startDate = null, string startTime = null, DateTime? endDate = null, string endTime = null, int? pageSize = null, string nextPage = null)
        {
            ApiResponseSecurityIntradayPrices response = null; 
            try
            {
                response = securityApi.GetSecurityIntradayPrices(identifier, source, startDate, startTime, endDate, endTime, pageSize, nextPage);
                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception when calling SecurityApi.GetSecurityStockPrices: " + e.Message + response);
                return response;
            }

        }
        public RealtimeStockPrice GetSecurityRealtimePrice(string identifier, string source = null)
        {
            try
            {
                return securityApi.GetSecurityRealtimePrice(identifier, source);
                
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception when calling SecurityApi.GetSecurityRealtimePrice: " + e.Message);
                return null;
            }
        }
    }
}