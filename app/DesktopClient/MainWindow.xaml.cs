﻿using DesktopClient.ViewModel;
using System.Windows;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using AlphaVantage.Net.Stocks.TimeSeries;
using DesktopClient.AlphaVDataProvider;
using DesktopClient.Financials;
using ServiceStack;
using System.Data;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Input;
using System.Windows.Controls;
using Intrinio.SDK.Model;

namespace DesktopClient
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }

        private async void OnButton_Clicked(object sender, RoutedEventArgs e)
        {
            if(DataContext is MainWindowViewModel viewModel)
            {
                await viewModel.StopButton();
            }
        }

        private void ResetZoom()
        {
            CartesianChart.AxisX[0].MinValue = double.NaN;
            CartesianChart.AxisX[0].MaxValue = double.NaN;
            CartesianChart.AxisY[0].MinValue = double.NaN;
            CartesianChart.AxisY[0].MaxValue = double.NaN;
        }

        private async Task StopPreviousChartUpdate(MainWindowViewModel viewModel)
        {
            await viewModel.StopChartUpdate();
        }

        private void UpdateInterfaceIntrinioData(MainWindowViewModel viewModel)
        {
            var selectedStockTicker = (stockList.SelectedItem as CustomStockDataPoint).Symbol;
            ApiResponseSecurityStockPrices savedStockData = viewModel.FindStockDataIntrinio(selectedStockTicker);
            if (savedStockData != null)
            {
                ResetZoom();
                viewModel.PlotData(savedStockData);
              //  viewModel.StartWindowDataUpdater(selectedStockTicker, stockList.SelectedItem);
            }
            else
            {
                Console.WriteLine("Stock data not found in saved stock list!");
            }
        }

        private void UpdateInterfaceAlphaVintageData(MainWindowViewModel viewModel)
        {
            var selectedStockTicker = (stockList.SelectedItem as CustomStockDataPoint).Symbol;
            StockTimeSeries savedStockData = viewModel.FindStockDataAlphaVintage(selectedStockTicker);
            if (savedStockData != null)
            {
                ResetZoom();
                viewModel.PlotData(savedStockData);
                viewModel.StartWindowDataUpdater(selectedStockTicker, stockList.SelectedItem);
            }
            else
            {
                Console.WriteLine("Stock data not found in saved stock list!");
            }
        }
        private async void StockList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (DataContext is MainWindowViewModel)
            {
                var viewModel = (MainWindowViewModel)DataContext;
                await StopPreviousChartUpdate(viewModel);
                if(viewModel.UseIntrinioApi)
                    UpdateInterfaceIntrinioData(viewModel);
                else
                    UpdateInterfaceAlphaVintageData(viewModel);
            }
        }
    }
}
