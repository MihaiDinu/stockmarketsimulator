﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.ComponentModel;
using LiveCharts;
using DesktopClient.AlphaVDataProvider;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Threading;
using AlphaVantage.Net.Stocks.TimeSeries;
using LiveCharts.Wpf;
using LiveCharts.Geared;
using DesktopClient.Financials;
using System.Threading.Tasks.Dataflow;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Collections.ObjectModel;
using Intrinio.SDK.Model;
using ServiceStack;
using System.Printing;
using Newtonsoft.Json;
using System.Media;

namespace DesktopClient.ViewModel
{
    public partial class MainWindowViewModel : INotifyPropertyChanged
    {

        private SeriesCollection _seriesCollection;
        public SeriesCollection SeriesCollection
        {
            get => _seriesCollection;
            set
            {
                _seriesCollection = value;
                OnPropertyChanged(nameof(SeriesCollection));
            }
        }

        private List<string> _labels;
        public List<string> Labels
        {
            get => _labels;
            set
            {
                _labels = value;
                OnPropertyChanged(nameof(Labels));
            }
        }

        private List<CustomStockDataPoint> _items;
        public List<CustomStockDataPoint> Items
        {
            get => _items;
            set
            {
                _items = value;
                OnPropertyChanged(nameof(Items));
            }
        }

        private Func<double, string> _yFormatter;
        public Func<double, string> YFormatter
        {
            get => _yFormatter;
            set
            {
                _yFormatter = value;
                OnPropertyChanged(nameof(YFormatter));
            }
        }
        public bool UseIntrinioApi { get; set; }

        private AlphaVintageStockDataProvider stockDataAlpha;
        private IntrinioStockDataProvider stockDataIntrinio;
        private readonly HashSet<string> consumeableStockStringList;
        private readonly HashSet<string> checkedStockStringList;
        private readonly List<StockTimeSeries> stockTimeSeriesList;
        private readonly List<ApiResponseSecurityStockPrices> stockApiResponsePricesList;
        private readonly List<StockPriceSummary> stockPriceSummaryList;
        private Timer chartUpdater;
        private Timer StockGenerator;

        public MainWindowViewModel()
        {
            stockDataAlpha = new AlphaVintageStockDataProvider();
            stockDataIntrinio = new IntrinioStockDataProvider();
            stockTimeSeriesList = new List<StockTimeSeries>();
            stockApiResponsePricesList = new List<ApiResponseSecurityStockPrices>();
            stockPriceSummaryList = new List<StockPriceSummary>();
            checkedStockStringList = new HashSet<string>();
            _items = new List<CustomStockDataPoint>();
            consumeableStockStringList = new HashSet<string>{
              "AAPL",
              "CAT",
              "BA",
              "WMT",
              "IBM",
              "XOM",
              "FCA",
              "AXP",
              "PFE",
              "NKE",
              "MCD",
              "JNJ",
              "HD",
              "KO",
              "MMM",
              "MSFT",
              "V",
              "CVX",
              "CSCO",
              "GS",
              "INTC",
              "JPM",
              "MRK",
              "PG",
              "UNH",
              "TRV"
            };
           
            UseIntrinioApi = true;

            if (!UseIntrinioApi)
            {
                StockGenerator = new Timer(
                    async e => await GenerateStockListAsync(),
                    null,
                    TimeSpan.FromSeconds(0),
                    TimeSpan.FromMinutes(1));
            }
            else
            {
                Task.Run(async () =>
                {
                    await GenerateStockListIntrinio();
                });
            }
        }
        public async Task GenerateStockListIntrinio()
        {
            System.Diagnostics.Stopwatch watch;
            try
            {
                foreach (string ticker in consumeableStockStringList)
                {
                    watch = System.Diagnostics.Stopwatch.StartNew();
                    var stockDataProvider = new IntrinioStockDataProvider();
                    var stockData = stockDataProvider.GetSecurityStockPrices(ticker, frequency:"monthly");
                    var nextPage = stockData.NextPage;
                    int count = 1;
                    while (nextPage != null && count < 50)
                    {
                        var nextStockData = stockDataProvider.GetSecurityStockPrices(ticker, nextPage: nextPage);
                        foreach (var stock in nextStockData.StockPrices)
                            stockData.StockPrices.Add(stock);
                        nextPage = nextStockData.NextPage;
                        Console.WriteLine(stockData.Security.Name + " stock data points:" + stockData.StockPrices.Count);
                        count++;
                    }

                    var intradayStockPrices = stockDataProvider.GetSecurityIntradayPrices(ticker);

                    foreach (var stock in intradayStockPrices.IntradayPrices)
                        stockData.StockPrices.Add(new StockPriceSummary(stock.Time) { Close = stock.LastPrice });

                    stockData.StockPrices.Reverse();
                    foreach (var stock in stockPriceSummaryList)
                        stockData.StockPrices.Add(stock);

                    stockApiResponsePricesList.Add(stockData);
                    var lastStockData = stockData.StockPrices.OfType<StockPriceSummary>().LastOrDefault();

                    CustomStockDataPoint customStock = new CustomStockDataPoint()
                    {
                        Symbol = intradayStockPrices.Security.Ticker,
                        Price = lastStockData.Close ?? 0,
                        HighestPrice = lastStockData.High ?? 0,
                        LowestPrice = lastStockData.Low ?? 0,
                        TradingTime = lastStockData.Date ?? default(DateTime)
                    };

                    customStock.SetPrecisionTo(2);

                    _items.Add(customStock);
                    Items = new List<CustomStockDataPoint>(_items);
                    checkedStockStringList.Add(customStock.Symbol);
                    Console.WriteLine(customStock.Symbol);

                    watch.Stop();
                    var elapsedMs = watch.ElapsedMilliseconds;
                    decimal elapsedSec = elapsedMs / 1000;
                    Console.WriteLine("It took:" + elapsedSec + "s to display " + customStock.Symbol + "'s data!");
                }

                if (consumeableStockStringList.IsEmpty())
                {
                    Console.WriteLine("Checked whole list!");
                    await StopStockGeneration();
                }
            }
            catch (NullReferenceException e)
            {
                SystemSounds.Hand.Play();
                removeConsumedStocks();
                Console.WriteLine("Checked list in Exception's catch!");
                Console.WriteLine("Request limit may have been exceeded!");
                Console.WriteLine(e.Message);
                await Task.Run(async () =>
                 {
                     await GenerateStockListIntrinio();
                 });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        async Task GenerateStockListAsync()
        {
            try
            {
                int count = 0;
                foreach (string ticker in consumeableStockStringList)
                {
                    var stockTimeSeries = await stockDataAlpha.GetIntradayTimeSeriesAsync(ticker);
                    stockTimeSeriesList.Add(stockTimeSeries);
                    var lastStockData = stockTimeSeries.DataPoints.OfType<StockDataPoint>().FirstOrDefault();
                    CustomStockDataPoint customStock = new CustomStockDataPoint()
                    {
                        Symbol = stockTimeSeries.Symbol,
                        Price = lastStockData.ClosingPrice,
                        HighestPrice = lastStockData.HighestPrice,
                        LowestPrice = lastStockData.LowestPrice
                    };

                    customStock.SetPrecisionTo(2);
                    _items.Add(customStock);
                    Items = new List<CustomStockDataPoint>(_items);
                    Console.WriteLine(stockTimeSeries.Symbol);

                    count++;
                    checkedStockStringList.Add(ticker);
                    if (count == 4)
                        break;
                }

                removeConsumedStocks();
                if (consumeableStockStringList.IsEmpty())
                {
                    Console.WriteLine("Checked list!");
                    await StopStockGeneration();
                }
            }
            catch (Exception e)
            {
                removeConsumedStocks();
                Console.WriteLine("Called too soon, request limit bay have been exceeded.");
                Console.WriteLine(e.Message);
            }
        }

        #region commented parallel stock generation block

        //try
        //{
        //    var getStockTimeSeries = new TransformBlock<string, StockTimeSeries>(
        //        async ticker =>
        //        {
        //            var stockTimeSeries = await stockData.GetDailyTimeSeriesAsync(ticker);
        //            stockTimeSeriesList.Add(stockTimeSeries);

        //            var lastStockData = stockTimeSeries.DataPoints.OfType<StockDataPoint>().FirstOrDefault();

        //            CustomStockDataPoint customStock = new CustomStockDataPoint()
        //            {
        //                Symbol = stockTimeSeries.Symbol,
        //                Price = lastStockData.ClosingPrice,
        //                HighestPrice = lastStockData.HighestPrice,
        //                LowestPrice = lastStockData.LowestPrice
        //            };

        //            customStock.SetPrecisionTo(2); 
        //            _items.Add(customStock);

        //            Items = new List<CustomStockDataPoint>(_items);

        //            return stockTimeSeries;

        //        }, new ExecutionDataflowBlockOptions
        //        {
        //            MaxDegreeOfParallelism = 1
        //        });

        //    var writeCustomerBlock = new ActionBlock<StockTimeSeries>(c => Console.WriteLine(c.Symbol));

        //    getStockTimeSeries.LinkTo(
        //        writeCustomerBlock, new DataflowLinkOptions
        //        {
        //            PropagateCompletion = true
        //        });

        //    int count = 0;
        //    foreach (var stock in stockStringList)
        //    {
        //        var res = getStockTimeSeries.Post(stock);
        //        Console.WriteLine(stock + " post is " + res + ", count: " + count);
        //        count++;
        //        checkedStockStringList.Add(stock);
        //        if (count == 5)
        //            break;
        //    }
        //    getStockTimeSeries.Complete();
        ////    writeCustomerBlock.Completion.Wait();

        //    foreach (var stock in checkedStockStringList)
        //        if (stockStringList.Contains(stock))
        //        {
        //            stockStringList.Remove(stock);
        //            Console.WriteLine(stock + " has been removed.");
        //        }

        //    checkedStockStringList.Clear();
        //    Console.WriteLine("Refresh?");

        //}
        //catch (NullReferenceException)
        //{
        //    Console.WriteLine("Data limit excedeed when generating stock list!");
        //}
        //catch (Exception e)
        //{
        //    Console.WriteLine("Error occured when generating stock list! Check parallel stock generation.");
        //    Console.WriteLine(e.Message);
        //}
        //} //end of bracket
       
        #endregion
        public void removeConsumedStocks()
        {
            foreach (var stock in checkedStockStringList)
                if (consumeableStockStringList.Contains(stock))
                {
                    consumeableStockStringList.Remove(stock);
                    Console.WriteLine(stock + " has been removed from consumable list.");
                }
            checkedStockStringList.Clear();
        }
    
        public async Task StopButton()
        {
            //IsRequesting = false;
            if (chartUpdater != null)
                await chartUpdater.DisposeAsync();
        } 
        public async Task StopStockGeneration()
        {
            if (StockGenerator != null)
                await StockGenerator.DisposeAsync();
            Console.WriteLine("Generation done");
        }


        public async Task StopChartUpdate()
        {
            if(chartUpdater != null)
                await chartUpdater.DisposeAsync();
        }

        public StockTimeSeries FindStockDataAlphaVintage(string ticker)
        {
           return stockTimeSeriesList.Find(x => x.Symbol.Contains(ticker));
        } 
        public ApiResponseSecurityStockPrices FindStockDataIntrinio(string ticker)
        {
           return stockApiResponsePricesList.Find(x => x.Security.Ticker.Equals(ticker));
        }

        public void PlotData(StockTimeSeries stock)
        {
            string ticker = stock.Symbol.ToString();

            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = ticker,
                    Values = new GearedValues<decimal>().AsGearedValues().WithQuality(Quality.Medium)
                }
            };

            Labels = new List<string>();
            var ordered = stock.DataPoints.OrderBy(dataPoint => dataPoint.Time).ToList();
            foreach (var data in ordered)
            {
                SeriesCollection[0].Values.Add(data.ClosingPrice);
                Labels.Add(data.Time.ToString("dd MMM yyyy hh:mm:ss"));    
            }

            YFormatter = value => value.ToString("C");
        }


        public void PlotData(ApiResponseSecurityStockPrices stock)
        {
            string ticker = stock.Security.Ticker;

            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = ticker,
                    Values = new GearedValues<decimal>().AsGearedValues().WithQuality(Quality.Medium)
                }
            };

            Labels = new List<string>();
            var ordered = stock.StockPrices.OrderBy(dataPoint => dataPoint.Date).ToList();
            foreach (var stockData in ordered)
            {
                SeriesCollection[0].Values.Add(stockData.Close);
                Labels.Add(stockData.Date?.ToString("dd MMM yyyy hh:mm:ss"));
            }

            YFormatter = value => value.ToString("C");
        }
        public CustomStockDataPoint GetLastestCustomStockData(string ticker)
        {
            CustomStockDataPoint lastestStockData = null;
            try
            {
                var lastStockData = stockDataAlpha.GetEndQuoteTimeSeries(ticker);
                var parsedStockData = JObject.Parse(lastStockData);
                var stockPrice = Convert.ToDecimal(parsedStockData["Global Quote"]["05. price"]);
                var high = Convert.ToDecimal(parsedStockData["Global Quote"]["03. high"]);
                var low = Convert.ToDecimal(parsedStockData["Global Quote"]["04. low"]);
                var changePercentage = parsedStockData["Global Quote"]["10. change percent"].ToString();
                var tradingTime = DateTime.Parse(parsedStockData["Global Quote"]["07. latest trading day"].ToString() + DateTime.Now.ToString(" hh:mm:ss"));

                lastestStockData = new CustomStockDataPoint()
                {
                    Symbol = ticker,
                    Price = stockPrice,
                    HighestPrice = high,
                    LowestPrice = low,
                    PercentageChange = changePercentage,
                    TradingTime = tradingTime
                };
            }
            catch (NullReferenceException)
            {
                throw;
            }

        return lastestStockData;
        }

        public void StartWindowDataUpdater(string latestStockTicker, object selectedItem)
        {
                chartUpdater = new Timer(
                    e => 
                    {
                        Console.WriteLine("Checking for update");
                        CustomStockDataPoint customStockData = null;
                        try
                        {
                             customStockData = GetLastestCustomStockData(latestStockTicker);
                        }
                        catch (NullReferenceException)
                        {
                            Console.WriteLine("Data limit excedeed.");
                        }
                        if(customStockData != null)
                        {
                            UpdateChart(customStockData);
                            UpdateDataGrid(customStockData, selectedItem);
                            Console.WriteLine("Updated chart");
                        }
                    },
                    null,
                    TimeSpan.FromSeconds(2),
                    TimeSpan.FromMinutes(1));
        }

        string StringPercentageFormatter(string percentage)
        {
            int precision = 2;
            var pieces = percentage.Split('%');
            var formattedDecimal = Math.Round(Convert.ToDecimal(pieces[0]), precision);
            return $"{formattedDecimal}%";
        }
        void UpdateDataGrid(CustomStockDataPoint customStockData, object selectedItem)
        {
            try
            {
                if(selectedItem is CustomStockDataPoint customStockDataPoint)
                {
                    customStockDataPoint.Price = Math.Round(customStockData.Price, 2, MidpointRounding.ToEven);
                    customStockDataPoint.LowestPrice = Math.Round(customStockData.LowestPrice, 2, MidpointRounding.ToEven);
                    customStockDataPoint.HighestPrice = Math.Round(customStockData.HighestPrice, 2, MidpointRounding.ToEven);
                    customStockDataPoint.PercentageChange = StringPercentageFormatter(customStockData.PercentageChange);
                }
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("UpdateDataGrid error occured!");
            }
        }

        public void UpdateChart(CustomStockDataPoint customStockData)
        {
            try
            {
                SeriesCollection[0].Values.Add(customStockData.Price);
                Labels.Add(customStockData.TradingTime.ToString("dd MMM yyyy hh:mm:ss"));
            }
            catch (Exception)
            {
                Console.WriteLine("Update Chart error occured!");
            }
        }

        //to be used only with the right data - yesterday closing price and today's closing price 
        public string calculatePercenageChange(decimal oldPrice, decimal NewPrice)
        {
            decimal change = 0;
            if (NewPrice > oldPrice)
                change = (NewPrice - oldPrice) / oldPrice * 100;
            else change = (oldPrice - NewPrice) / oldPrice * 100;

            return Math.Round(change, 2, MidpointRounding.ToEven).ToString() + "%";

        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
